
#include <stdlib.h>
#include <stdio.h>

 
int main(int argc, char** argv){
	
	if(argc % 2 != 0){
		printf("Format: <output_file_name> list(<val, numberOfTimesToRepeat>)\n");
		exit(1);
	}
	char *outputFile = argv[1];
        int valsAndLensLength = (argc-1)/2;
	size_t lens[valsAndLensLength];
	int vals[valsAndLensLength];

	//count vals
	size_t valIndex = 0;
	for(int i = 2; i < argc; i = i + 2){
		vals[valIndex++] = (int)strtol(argv[i], NULL, 0);
	}

        //count lens
        size_t lenIndex = 0;
        for(int i = 3; i < argc; i = i + 2){
                lens[lenIndex++] = (size_t)strtol(argv[i], NULL, 0);
        }

	FILE *f = fopen(outputFile, "w");
	if (f == NULL){
    		printf("Error opening file!\n");
    		exit(1);
	}
	
	for(int i = 0; i < valsAndLensLength; i++){
		int curVal = vals[i];
		size_t curLen = lens[i];
		for(size_t j = 0; j < curLen; j++){
			fwrite(&curVal, sizeof(int), 1, f);		
		}
	}
	fclose(f);

	return 0;
}
