## AvgFileFun README ##

I wrote a short and mildly interesting [article](http://spencer-carroll.com/more-threads-equals-more-speed-with-file-io/) at my website about this code.

To use the files first compile them. 

```
#!bash
gcc AvgFileCreator.c -o afCreator
#note the "++". This is likely different on systems other than mac os x
g++ AvgFileMulti.cpp -o afMulti -std=c++11
```



And then create some data files with afCreator. afCreator takes a outputfile name and a sequence of ints in the form:
```
#!bash
./afCreator <output_filename> <value> <times_to_repeat> <value> <times_to_repeat> ...
```

I suggest creating about 8 files to really test it:

```
#!bash
#about 76mb file. The average is of course 15.0
./afCreator t1.ff 10 10000000 20 10000000

./afCreator t2.ff 20 10000000 30 10000000
./afCreator t3.ff 30 10000000 40 10000000
./afCreator t4.ff 40 10000000 50 10000000

#about 7.6mb files
./afCreator t5.ff 50 1000000 60 1000000
./afCreator t6.ff 60 1000000 70 1000000
./afCreator t7.ff 70 1000000 80 1000000
./afCreator t8.ff 80 1000000 90 1000000
```


Then use afMulti to time how long it takes. You can supply afMulti with an argument of how many threads to use. afMulti will use one thread per file. The default number of threads is the number of logical cores your computer has.

Use the time program and ls to time and feed afMulti like so

```
#!bash
#4 threads
time ls *.ff | ./afMulti 4

#1 thread
time ls *.ff | ./afMulti 1
```



Thats it. It's pretty neat to see how the number of threads changes the outcome. Try commenting out the averaging for-loop in the AvgFileMulti.cpp to see how much faster it can go.