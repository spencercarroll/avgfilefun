
#include <string>
#include <vector>
#include <atomic>
#include <sstream>
#include <iostream>
#include <thread>

#include <pthread.h>
#include <stdlib.h>
#include <sys/stat.h>



std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
	std::stringstream ss(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
	return elems;
}

std::vector<std::string> split(const std::string &s, char delim) {
	std::vector<std::string> elems;
	split(s, delim, elems);
	return elems;
}

void streamToString(std::istream &is, std::string &str){

	std::string curLine;	

	while (std::getline(std::cin, curLine)){
		str.append(curLine);
		str.append("\n");
	}
}

std::vector<std::string> files;
std::atomic<int> nextFileIndex(0);
double *results;
typedef int DataType;

void runFileAverage(int n){

	int fileToRead = 0;
	DataType* data = NULL;
	while((fileToRead = nextFileIndex.fetch_add(1, std::memory_order_relaxed)) < files.size()){
		std::string fileStr = files[fileToRead];

		struct stat st;
		stat(fileStr.c_str(), &st);
		size_t inFileSize = st.st_size;

		FILE *inFile = fopen(fileStr.c_str(), "r");

		data = (DataType *)realloc(data, inFileSize);
		long dataLen = inFileSize/sizeof(DataType);
		fread(data, inFileSize, 1, inFile);

		//not the most precise way to take an average due to rounding
		double multiplier = 1.0/dataLen;
		double sum = 0;
		for(long i = 0; i < dataLen; i++){
			sum = sum + multiplier * data[i];
		}	
		results[fileToRead] = sum;
	}
	free(data);
}

int main(int argc, char** argv){

	
	
	std::string filesString;
	streamToString(std::cin, filesString);
	files = split(filesString, '\n');
	results = (double *)malloc(sizeof(double) * files.size());

	//Get the number of cores on the machine. Note this is not necessarily the number of physical cores.
	// More likely it is the number of "logical" cores. 
	unsigned int nthreads = std::thread::hardware_concurrency();	

	if(argc == 2){
		std::stringstream argStream((std::string(argv[1]))); 
		int x;  
		argStream >> x;  
		if (!argStream){      
			std::cout << "Could not parse threads-to-launch argument.\n";
			return 1;
		}
		nthreads = x;
	}
	std::vector<std::thread> threads; 
	for(int i = 0; i < nthreads; i++){
		threads.push_back(std::thread(runFileAverage, 0));
	}	
	
	for(int i = 0; i < nthreads; i++){
		threads[i].join();
	}
	std::cout << "<file>:<average>\n";	
	for(size_t i = 0; i < files.size(); i++){
		std::cout << files[i] << ":" << results[i]<< "\n";
	}
	free(results);
	return 0;
}
